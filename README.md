Title: Ball Game

Description: This is a proof of concept of the idea of using the movement of your head to control a game and interact with objects in the game.
This project originated from the idea of catching falling balls with a cup on your head. I was unable to use the box2d addon to catch shapes so 
instead I opted for deflecting the shapes off of each other. I would like to spend more time on this project building out the game and using the contact 
listeners and a counter to make the game have an objective. 

